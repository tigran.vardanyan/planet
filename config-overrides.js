const {
    alias
} = require('react-app-rewire-alias');

module.exports = function override(config) {
    alias({
        '@planetkit': 'src/components/system',
        '@components': 'src/components',
        '@styles': 'src/styles',
        '@assets': 'src/assets',
        '@store': 'src/store',
        '@hooks': 'src/hooks',
        '@decorators': 'src/decorators',
        '@constants': 'src/constants',
        '@helpers': 'src/helpers',
        '@utils': 'src/utils',
        '@types': 'src/types',
        '@api': 'src/api',
        '@screens': 'src/screens',
    })(config);

    return config;
};