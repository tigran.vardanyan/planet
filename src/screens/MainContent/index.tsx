import React, { FC } from 'react'
import PlanetCard from '@planetkit/PlanetCard';
import Button from '@planetkit/Button';
import './styles.css';
import { Content } from 'antd/lib/layout/layout';
import { Col, Row } from 'antd';



const MainContent: FC<any> = ({ title, cards, categoryId }) => {

  const chunkSize = 8
  const renderData = [];
  for (let i = 0; i < cards.length; i += chunkSize) {
    const chunk = cards.slice(i, i + chunkSize);
    renderData.push(chunk);
  }

  return (<>
    <Row className='title-content'  >
      <Button type='text' className='standard' > {title}</Button>
    </Row>
    <Content>
      <Row className='main-content' justify='space-between' gutter={[20, 20]}>
        {renderData.map((renderCards: any, index: number) =>
          renderCards.map((card: any) =>
            <Col key={card.id} className='card-content'>
              <PlanetCard card={card} categoryId={categoryId} />
            </Col>
          )
        )}
      </Row>
    </Content>
  </>

  )
}

export default MainContent