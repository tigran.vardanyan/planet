import AboutUsService from '@api/services/AboutUsService';
import { useMount } from '@hooks/useMount';
import { Col } from 'antd';
import React, { FC, useState } from 'react'
import * as styles from './styles';


const AboutUs: FC<any> = () => {

  const [data, setData] = useState<any>({})
  useMount(() => {
    AboutUsService.get().then((res) => {
      setData(res.data)})
  })


  return (
    <styles.wrapper justify='start'>
      <Col span={24} >
        <h1 className='about-header'>About Us</h1>
      <p className='about-text'>
        {data?.text}
      </p>
      </Col>

    </styles.wrapper>

  )
}

export default AboutUs;