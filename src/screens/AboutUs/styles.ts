import styled from '@styles';

import { Row as AntRow } from 'antd';

export const wrapper = styled(AntRow)`

background-color:#fff;
text-align: justify;

.about-header{
  font-weight: 800;
  font-size: 34px;
  line-height: 41px;
}
.about-text{
  font-weight: 300;
  font-size: 22px;
  line-height: 48px;
}
`
