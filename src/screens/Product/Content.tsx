import Button from '@planetkit/Button';
import { Col, Row } from "antd";
import React, { FC } from "react";
import './content.css'

const Content: FC<any> = ({ data }) => {


  return (
    <Col md={24} lg={14} xl={16} className='product-content'>
      <Row justify='start'>
        <Col span={24} className='content-item'><h1>{data?.title}</h1></Col>
        {data?.price && <Col span={24} className='content-item'>
          <span className="price">
            {data?.price} AMD
          </span>
        </Col>}
        <Col span={24} className='content-item'>
          {data?.availability ? <span className='status' aria-disabled>Available</span> : <span className='status status-disable' aria-disabled>Unavailable</span>}
        </Col>
        {data?.color && <Col span={24} className='content-item'>
          <span className="price">
            {data?.color}
          </span>
        </Col>}
        <Col span={24} className='content-item'>
          <Row>
            <Col>
              <Button type="primary" disabled> Buy Now </Button>
            </Col>
            <Col className='add-basket' >
              <Button type='primary' className='white' disabled > Add To Basket </Button>
            </Col>

          </Row>
        </Col>
        {data?.description1 && <Col span={24} className='content-item'>
          <span >
            {data?.description1}
          </span>
        </Col>}
      </Row>
    </Col>
  )
}

export default Content