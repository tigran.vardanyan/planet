import React, { FC, useEffect, useMemo, useState } from "react";
import ProductsService from "@api/services/ProductsService";
import MainContent from "@screens/MainContent";
import { Col,Row } from "antd";

import Carousel from "./Carousel";
import Content from "./Content";
import Descriptions from "./Descriptions";


import './product.css'
import Loader from "@screens/Loader";

const Product: FC<any> = ({ match }) => {

  const propertiesColumns = [
    {
      title: 'Attribute',
      dataIndex: 'attribute',
      key: 'attribute',
    },
    {
      title: 'Value',
      dataIndex: 'value',
      key: 'value',
    }
  ];

  const descriptionsColumns = [
    {
      title: '',
      dataIndex: 'descriptions',
      key: 'descriptions',
    }
  ];

  const [data, setData] = useState<any>({})
  const [cardsData, setCardsData] = useState<any>([])

  useEffect(() => {
    ProductsService.getById(match.params.id).then(res => {
      setData(res.data)
    })


  }, [match.params.id])

  useEffect(() => {
    ProductsService.getByCategoryIds(53).then(res => {


      setCardsData((res.data?.splice(1, res.data.length - 1)))
    })
  }, [match.params.id])

const propertiesDataSource = data.attributes?.map(({description, product_attribute}:any)=> {
    return {
       attribute:description, 
       value: product_attribute?.value?.value
     }
   })

const descriptionsDataSource = [{
  descriptions: data.description2
}]


  return <>
    <Row className="products-row">
      <Col span={24} className="products-col">
        {(!data || !cardsData) ? <Loader /> :

          <Row justify="center">

            <Carousel data={data?.files} />
            <Content data={data} />
            <Descriptions dataSource={propertiesDataSource} columns={propertiesColumns} title={"Properties"}/>
            
            { data.description2!=="" && <Descriptions dataSource={descriptionsDataSource} columns={descriptionsColumns} title={"Descriptions"} />}
            <Col span={24} style={{ marginTop: "20px" }}>
              <MainContent title='Similar Products' cards={cardsData} />
            </Col>
          </Row>
        }

      </Col>
    </Row>



  </>
}

export default Product