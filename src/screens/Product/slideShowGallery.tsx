import React, { FC} from "react";

import pic1 from './Rectangle193.jpg'
import pic2 from './Rectangle194.jpg'

import './styles.css'

const SlideShowGallery: FC<any> = () => {

  let slideIndex = 1;
  showSlides(slideIndex);

  function plusSlides(n: number) {
    showSlides(slideIndex += n);
  }

  function currentSlide(n: number) {
    showSlides(slideIndex = n);
  }

  function showSlides(n: number) {
    let i;
    let slides = document.getElementsByClassName("mySlides");
    let dots = document.getElementsByClassName("demo");
    let captionText = document.getElementById("caption");
    if (n > slides.length) {slideIndex = 1}
    if (n < 1) {slideIndex = slides.length}
    for (i = 0; i < slides.length; i++) {
      // slides[i].style.display = "none";
       

    }
    for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
    }
    // slides[slideIndex-1].style.display = "block";
    // dots[slideIndex-1].className += " active";
    // captionText.innerHTML = dots[slideIndex-1].alt;
  }


  return <>

    <div className="container">

      <div className="mySlides">
        <div className="numbertext"
        style={{ backgroundImage: `url(${pic1})`, width:"30%", height:"25%"}}
        >1</div>
        {/* <img alt="1" src="./Rectangle193.jpg" style={{ "width": "100%" }} /> */}
      </div>

      <div className="mySlides">
      <div className="numbertext"
        style={{ backgroundImage: `url(${pic2})`,  width:"30%", height:"25%"}}
        >2</div>
        {/* <img alt="2" src="Rectangle194.jpg" style={{ "width": "100%" }} /> */}
      </div>

      {/* <div className="mySlides">
        <div className="numbertext">3 / 6</div>
        <img alt="yyy" src="img_mountains_wide.jpg" style={{ "width": "100%" }} />
      </div>

      <div className="mySlides">
        <div className="numbertext">4 / 6</div>
        <img alt="yyy" src="img_lights_wide.jpg" style={{ "width": "100%" }} />
      </div>

      <div className="mySlides">
        <div className="numbertext">5 / 6</div>
        <img alt="yyy" src="img_nature_wide.jpg" style={{ "width": "100%" }} />
      </div> */}

      {/* <div className="mySlides">
        <div className="numbertext">6 / 6</div>
        <img alt="yyy" src="img_snow_wide.jpg" style={{ "width": "100%" }} />
      </div> */}

      <button className="prev" onClick={() => plusSlides(-1)}>&#10094;</button>
      <button className="next" onClick={() => plusSlides(1)}>&#10095;</button>

      <div className="caption-container">
        <p id="caption"></p>
      </div>

      <div className="row"   style={{ height:"100px"}}>
        <div className="column" style={{height:"100%"}}>
        <div className="demo cursor"
        style={{ backgroundImage: `url(${pic1})`, width:"100%", height:"100%"}}
        onClick={() => currentSlide(1)}
        />
        </div>
        <div className="column" style={{height:"100%"}}>
        <div className="demo cursor"
        style={{ backgroundImage: `url(${pic1})`, width:"100%", height:"100%"}}
        onClick={() => currentSlide(2)}
        />
        </div>
        {/* <div className="column">
          <img className="demo cursor" src="img_mountains.jpg" style={{ "width": "100%" }} onClick={() => currentSlide(3)} alt="Mountains and fjords" />
        </div>
        <div className="column">
          <img className="demo cursor" src="img_lights.jpg" style={{ "width": "100%" }} onClick={() => currentSlide(4)} alt="Northern Lights" />
        </div>
        <div className="column">
          <img className="demo cursor" src="img_nature.jpg" style={{ "width": "100%" }} onClick={() => currentSlide(5)} alt="Nature and sunrise" />
        </div>
        <div className="column">
          <img className="demo cursor" src="img_snow.jpg" style={{ "width": "100%" }} onClick={() => currentSlide(6)} alt="Snowy Mountains" />
        </div> */}
      </div>
    </div>


  </>
}

export default SlideShowGallery