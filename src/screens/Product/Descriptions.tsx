import { Col, Collapse, Table } from "antd";
import React, { FC } from "react";

const Descriptions: FC<any> = ({ dataSource ,columns,title}) => {
  const { Panel } = Collapse;
 
  return (
    <Col span={24} style={{marginTop:"20px"}} >

      <Collapse expandIconPosition={'end'} ghost className='properties-parent' defaultActiveKey={title}>
        <Panel header={title} key={title} >
         { columns.length > 1 ? 
          <Table dataSource={dataSource} columns={columns}  pagination={false} bordered style={{color:"red"}} />
          :
          <p>{dataSource.descriptions}</p>
         }
        </Panel>
      </Collapse>


    </Col>
  )
}

export default Descriptions