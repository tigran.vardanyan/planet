import React, { FC, useEffect, useState } from 'react'
import MainContent from '../../MainContent';
import cableSrc from '../../../assets/images/cable.svg'
import ProductsService from '@api/services/ProductsService';


type card = {
  price: number
}

const Products: FC<any> = ({match}) => {


  const [cardsData, setCardsData] = useState<card[]>([])


  useEffect(() => {
    ProductsService.getByCategoryIds(match.params.id).then(res =>{
      
      setCardsData(([...res.data]))})
  },[match.params.id])


  return (
    <>

      <MainContent title='All' categoryId={match.params.id} cards={cardsData} imgSrc={cableSrc} />
      {/* <Sort cards={cards} setCards={setCards} /> */}
    </>
  )
}

export default Products;
