import styled from '@styles';

import { BRAND_CLASS_NAME } from '@styles/constants';

import { Carousel as AntCarousel } from 'antd';

export const Carousel = styled(AntCarousel)`

width: 500px;
height: 300px;
border: 5px solid;

.slick-list {
  width: inherit;
  height: inherit;
  background: green;
}
.slick-track {
  width: 500px !important;
  height: 300px;
  position: relative;
  left: 500px;
  background-color: red;

  .slick-slide {
    background-color: green;
  } 
  .slick-active{
    background-color: blue;
  }
}

`
