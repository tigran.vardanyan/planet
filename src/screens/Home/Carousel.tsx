import { BASE_URL } from "@api/config";
import React, { FC, useEffect } from "react";
// import "antd/dist/antd.css";

import NoImage from '@assets/images/NoImage.png'

import "./index.css";


interface SliderProps {
  data:{
    file: {
      id:string;
      path: string;
      name: string;
      extension: string

    };
    name:  string;
    title: string;
    type: string
  }[]
}

const delay = 15000;



const Carousel:FC<SliderProps> = ({data}) =>{
  const [index, setIndex] = React.useState(0);
  const timeoutRef = React.useRef(null);

  const  getImagePath = (file:any)=>{
    return file? `${BASE_URL}/${file.path}/${file.name}${file.extension}`: NoImage
  }

  function resetTimeout() {
    if (timeoutRef.current) {
      clearTimeout();
    }
  }

  useEffect(() => {
    resetTimeout();
     setTimeout(
      () =>
        setIndex((prevIndex) =>
          prevIndex === data.length - 1 ? 0 : prevIndex + 1
        ),
      delay
    );

    return () => {
      resetTimeout();
    };
  }, [index,data.length]);

  return (
    <div className="slideshow" ref={timeoutRef}>
      <div
        className="slideshowSlider"
        style={{ transform: `translate3d(${-index * 100}%, 0, 0)` }}
      >
        {data.map(({file}) => (
          <img src={getImagePath(file)}  key={file.id} alt="planet-carousel" className="slide"/>
        ))}
      </div>

      <div className="slideshowDots">
        {data.map((_, idx) => (
          <div
            key={`${idx}-dot`}
            className={`slideshowDot${index === idx ? " active" : ""}`}
            onClick={() => {
              setIndex(idx);
            }}
          ></div>
        ))}
      </div>
    </div>
  );
}


export default Carousel