/* eslint-disable max-classes-per-file */
import { errorsAdapter } from './errorsAdapter';

export class ResponseError extends Error {
  public response: Response;

  constructor(response: Response) {
    super(response.statusText);
    this.response = response;
  }
}

export class ServerError extends ResponseError {}

export class UnprocessableEntityError extends ResponseError {
  public errorMessage = '';
  public errors: any = null;

  constructor(response: any, info: any) {
    super(response);
    this.errors = info.errors ? errorsAdapter(info.errors) : null;
    this.errorMessage = info.message ?? '';
  }
}

export class UnauthorizedError extends ResponseError {
  public errorMessage = '';
  public errors: any = null;

  constructor(response: any, _info: any) {
    super(response);
    this.errorMessage = response.statusText ?? '';
  }
}
