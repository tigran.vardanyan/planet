type ErrorFieldData = {
  touched: boolean;
  validating: boolean;
  errors: string[];
  name: any;
};

/**
 * adapt server errors to ant form setFields format, to show errors to user
 * @param errors errors from server
 */
export const errorsAdapter = (errors: any): ErrorFieldData[] =>
  Object.values(errors).reduce(
    (acc: any[], { msg, param: name }: any) => [...acc, { name, errors: [msg], validating: true }],
    [],
  );
