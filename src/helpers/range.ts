function range(...args: number[]) {
  let start: number;
  let end: number;
  let step: number;

  const numbers: number[] = [];

  switch (args.length) {
    case 0:
      throw new Error('no arguments');
    case 1:
      start = 0;
      [end] = args;
      step = 1;
      break;
    case 2:
      [start, end] = args;
      step = 1;
      break;
    default:
      [start, end, step] = args;
      break;
  }

  for (let i = 0, num = start; num < end; num += step, i++) {
    numbers[i] = num;
  }

  return numbers;
}

export default range;
