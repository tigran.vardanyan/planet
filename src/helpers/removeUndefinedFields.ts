export const removeUndefinedFields = (data: any) => {
  const obj: any = {};

  if (data) {
    // Delete undefined props
    for (const key in data) {
      if (data[key]) {
        obj[key] = data[key];
      }
    }
  }

  return obj;
};
