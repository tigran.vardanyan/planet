import { request } from '@utils/request';
import { ABOUTUS_URL } from '@api/config';
// import { tryCatchProxy } from '../../decorators/tryCatchProxy';
import { tryCatchProxy } from '@decorators/tryCatchProxy';


@tryCatchProxy
class PartnersService {
  async get() {
    return request(`${ABOUTUS_URL}`, { method: 'GET' });
  }

}

export default new PartnersService();
