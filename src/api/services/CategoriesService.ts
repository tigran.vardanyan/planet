import { request } from '@utils/request';
import { CATEGORIES_URL } from '@api/config';
import { tryCatchProxy } from '@decorators/tryCatchProxy';


@tryCatchProxy
class CategoriesService {
  categories: any[] = [];

  // async createCategory(body: any) {
  //   const { data } = await request(CATEGORIES_URL, {
  //     method: 'POST',
  //     body: JSON.stringify(body),
  //     headers: {
  //       accept: 'application/json',
  //       'Content-Type': 'application/json',
  //     },
  //   });

  //   return data;
  // }

  async getById(id: string | number) {
    return request(`${CATEGORIES_URL}/${id}`, { method: 'GET' });
  }

  async getCategories() {
    if (this.categories.length) {
      return this.categories;
    }

    const { data } = await request(
      `${CATEGORIES_URL}?${new URLSearchParams({
        limit: '1000',
      })}`,
    );

    this.categories = data;
    return data;
  }



  // async deleteCategories(id: string | number) {
  //   await request(`${CATEGORIES_URL}/${id}`, { method: 'DELETE' });
  // }

  // async editCategories(id: string | number, body: any) {
  //   const { data } = await request(`${CATEGORIES_URL}/${id}`, {
  //     method: 'PATCH',
  //     body: JSON.stringify(body),
  //     headers: {
  //       accept: 'application/json',
  //       'Content-Type': 'application/json',
  //     },
  //   });

  //   return data;
  // }
}

export default new CategoriesService();
