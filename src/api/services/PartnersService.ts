import { request } from '@utils/request';
import { PARTNERS_URL } from '@api/config';
// import { tryCatchProxy } from '../../decorators/tryCatchProxy';
import { tryCatchProxy } from '@decorators/tryCatchProxy';


@tryCatchProxy
class PartnersService {
  async get() {
    return request(`${PARTNERS_URL}`, { method: 'GET' });
  }

}

export default new PartnersService();
