import { request } from '@utils/request';
import { PRODUCT_URL } from '@api/config';
// import { tryCatchProxy } from '../../decorators/tryCatchProxy';
import { tryCatchProxy } from '@decorators/tryCatchProxy';


@tryCatchProxy
class ProductsService {

  async getById(id: string | number) {
    
    return request(`${PRODUCT_URL}/${id}`, { method: 'GET' });
  }

  async getByCategoryIds(ids: string | number) {
    
    return request(`${PRODUCT_URL}?categoryIds=${ids}`, { method: 'GET' });
  }

}

export default new ProductsService();
