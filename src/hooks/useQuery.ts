import { useEffect, useRef, useState } from 'react';

export const useQuery = <T = any>(
  service: any,
  method: string = 'get',
  options?: { args?: any[]; init?: any; serialize?: (data: any) => any },
) => {
  const state = useRef<{ data: T | null; loading: boolean; error: any }>({
    data: null,
    loading: true,
    error: null,
  });

  const [queryStatus, setQueryStatus] = useState<'pending' | 'resolved' | 'rejected'>('pending');

  useEffect(() => {
    service[method](...(options?.args ?? []))
      .then((data: T) => {
        state.current = {
          data: options?.serialize?.(data) ?? data,
          loading: false,
          error: null,
        };
        setQueryStatus('resolved');
      })
      .catch((error: any) => {
        state.current = {
          data: options?.init ?? null,
          loading: false,
          error,
        };
        setQueryStatus('rejected');
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return {
    ...state.current,
    status: queryStatus,
  };
};
