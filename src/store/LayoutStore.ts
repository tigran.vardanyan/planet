import type { RootStore } from '@store';
import { action, makeObservable, observable } from 'mobx';

class LayoutStore {
  root: RootStore;

  isOpenService = false;
  isDisableContextual = false;

  constructor(root: RootStore) {
    this.root = root;

    makeObservable(this, {
      isOpenService: observable,
      isDisableContextual: observable,
      openService: action.bound,
      hideService: action.bound,
      changeDisableContextualNavigation: action.bound,
    });
  }

  openService() {
    this.isOpenService = true;
  }

  hideService() {
    this.isOpenService = false;
  }

  changeDisableContextualNavigation(flag: boolean) {
    this.isDisableContextual = flag;
  }
}

export default LayoutStore;
