/* eslint-disable no-param-reassign */
import { action, computed, makeObservable, observable } from 'mobx';

import { ThemeModeType, themes } from '@styles/theme/themes';
import type { RootStoreType } from '.';

class ThemeStore {
  root: RootStoreType;

  themeMode: ThemeModeType;

  constructor(root: RootStoreType) {
    this.root = root;

    this.themeMode = this.getThemeFromStorage() ?? 'light';

    makeObservable(this, {
      themeMode: observable,
      changeTheme: action,
      theme: computed,
    });
  }

  get theme() {
    return themes[this.themeMode];
  }

  toggleTheme() {
    this.changeTheme(this.themeMode === 'dark' ? 'light' : 'dark');
  }

  changeTheme(theme: ThemeModeType) {
    this.saveTheme(theme);
    this.themeMode = theme;
  }

  saveTheme(theme: ThemeModeType) {
    window.localStorage && localStorage.setItem('selectedTheme', theme);
  }

  getThemeFromStorage(): ThemeModeType | null {
    return window.localStorage
      ? (localStorage.getItem('selectedTheme') as ThemeModeType) || null
      : null;
  }
}

export default ThemeStore;
