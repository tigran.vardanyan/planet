// Safari doesn't support `buttons` property that contains bit mask of pressed mouse buttons.
// Instead it provides `which` property that fits to our needs.
export type ExtendedMouseEvent = MouseEvent & {
  which: number | null | undefined;
};
