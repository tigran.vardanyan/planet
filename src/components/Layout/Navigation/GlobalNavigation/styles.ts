import styled, { zIndex } from '@styles';

import { Header as AntHeader } from 'antd/lib/layout/layout';
export const Wrapper = styled(AntHeader)`


width: 100%;
height: 126px;
background: rgba(209, 211, 212,0.7);
border-radius: 30px 30px 0px 0px;
padding: 0 20px;



.nav-bar{
  /* background: rgba(209, 211, 212, 0.7);
  */
  /* background: rgba(209, 211, 212); */
  /* border-radius: 30px 30px 0px 0px; */
  height: 55%;
}


    
    .login-content{
      height: 100%;
      margin-top: 13px;
      
      .logo {
        /* position: relative;
        justify-content: start;
        display: flex;
        left: 20px;
        top: 20px;
        z-index: ${zIndex.logo}; */
        width: 100%;
        max-width: 233px;
        height: 45px;

      }
      
      .search-group {
        /* z-index: ${zIndex.serviceNav};
        position: relative;
        bottom: 34px; */



        .search{
          /* width:72% */
          max-width: unset;
          width: 100%;
          height: 45px;
        }
 

        .search-btn {
          /* width: 12% ; */
          height: 45px;
          border-radius: 0px 50px 50px 0px;
          /* position: relative;
          top: 5px; */

          max-width: 126px;
          width: 100%;
          height: 45px;
          left: 5px;
          top: 2px;
        } 

      }
        .register-group {
          /* position: relative;
          display: inline-flex;
          bottom: 79px;
          left: 42%;
          z-index: ${zIndex.serviceNav}; */

          .sign-up {
            /* margin-right: 15px; */
            /* line-height: 21px;
            text-decoration-line: underline;   */
            max-width: 126px;
          width: 100%;
          height: 45px;

          }
          .sign-in {
        
            /* width: 126px; */
            
            width: 100%;
            max-width: 126px;
            min-width: 120px;
            height: 70%;
            max-height: 45px;
            border-radius: 50px;
  
        }



  }

    }
    .menu-content {
      /* height: 35%;
      margin-right: 5px;
      display: flex;
      justify-content: end; */
      margin-top: -6px;

      ul {
        /* z-index: ${zIndex.serviceNav}; */
      }
      .menu-item {
        /* margin-right: 15px; */
      font-weight: bold;
      font-size: 18px;
      line-height: 21px;
      text-decoration-line: none;
    }
  
    .menu-item :hover:not(:disabled):not(.loading) {
      color: #00909E;
      text-decoration-line: underline;
      opacity: 1;
      cursor: pointer;

    }

    .menu-item :active:not(:disabled):not(.loading) {
      cursor: pointer;
      color: #00909E;

    }
    }



    .bars-menu {
      bottom: 10px;
    } 

    .small-logo{
      top: 7px;
    }

    .sign-up-small{
      bottom: 5px;
    }

    .responsive-bar-actions{
      display: none;
    }
    .small-search-content {
      bottom: 33px;
    }


    .search-small{
     
      width: 100%;
      height: 22px;
      top: 4px;
      right: 5px;
      
      ::placeholder {
        font-size: 12px;
    }
    }
 
.search-btn{
  height: 22px;
  border-radius: 0px 50px 50px 0px;
  max-width: 126px;
  width: 100%;
}


@media (max-width: 768px) {
  height: 85px;
  .responsive-bar-actions{
      display: flex;
    }
    
  .logo{
    display: none;
  }
  .top-menu,
  .bottom-menu {
    display: none;
  }

}
`;
