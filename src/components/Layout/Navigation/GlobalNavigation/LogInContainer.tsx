import React from 'react';
import { Link } from "react-router-dom";

import { Menu, Col, Row } from "antd";
import logo from "@assets/images/logo.png";

import Button from '@planetkit/Button';
import { Input } from '@planetkit/Input/styles';


const LoginContainer = () => {

  return (

    <Menu
      className="nav-bar"
      mode="horizontal"
      defaultSelectedKeys={["1"]}
      disabledOverflow={true}
    >


      <Row className='login-content' align="middle">
        <Col sm={24} md={6} lg={8} xl={6}  >

          <Menu.Item key="1" className="logo">
            <Link to="/" >
              <img src={logo} alt="logo" />
            </Link>
          </Menu.Item>
        </Col>
        <Col xs={14} sm={14} md={10} lg={8} xl={12} className='search-group' >
          <Menu.Item key='2'>

            <Row >
              <Col span={20} className='search-content'>
                <Input
                  className='search'
                  type='search'
                  autoComplete='off'
                  placeholder='Search'
                // onChange={e => search(e.target.value)}
                />
              </Col>
              <Col span={4} >
                <Button type='primary' className='search-btn ' icon='search' />
              </Col>
            </Row>
          </Menu.Item>
        </Col>
        <Col xs={10} sm={10} md={8} lg={8} xl={6} className=' register-group' >
          <Menu.Item key='3'>
            <Row style={{ float: 'right' }} >
              <Col span={12} >
                < Button type='text' className='sign-up' disabled >
                  Sign Up
                </Button>
              </Col>
              <Col span={12}>
                <Button type='primary' className='sign-in' disabled >
                  Sign In
                </Button>
              </Col>
            </Row>

          </Menu.Item>

        </Col>
      </Row>

    </Menu>

  )
}

export default LoginContainer