import React, { FC, useRef } from 'react';
import { Link } from "react-router-dom";

import { Menu, Col, Row } from "antd";

import Button from '@planetkit/Button';

import './responsiveBar.css';


interface MenuProps {
  tabs:
  {
    name: string;
    tab: string;
  }[]
  close:Function
}


const ResponsiveBar: FC<MenuProps> = ({ tabs,close }) => {
  const ref = useRef<HTMLDivElement>(null);


  return (
    <div ref={ref} className='responsive-bar-content'>

      <Menu
        className=" responsive-bar-content"
        mode="horizontal"
        defaultSelectedKeys={["1"]}
        disabledOverflow={true}
      >
        <Row >
          {tabs.map(({ name, tab }) => (
            <Menu.Item key={`${tab}-tab`} className='responsive-bar-item' >
              <Col span={24} onClick={()=>close()}  >
                <Link to={`/${tab}`}  >
                  < Button type='text' className='responsive-bar-btn'>
                    {name}
                  </Button>
                </Link>
              </Col>
            </Menu.Item>
          ))}
        </Row>


      </Menu>
    </div>
  )
}

export default ResponsiveBar