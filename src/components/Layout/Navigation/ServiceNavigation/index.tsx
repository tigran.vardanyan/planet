import React, { FC, useRef, useState } from 'react';
import { observer } from 'mobx-react-lite';
import Categories from './Categories'

import { useOutsideClickClose } from '@hooks/useOutsideClickClose';
import CategoriesService from '@api/services/CategoriesService';
import { useMount } from '@hooks/useMount';
import { useStores } from '@store';

import './drawer.css'
import './index.css'
import './categoriesResponsive.css'

// const tabs = [
//   { name: 'Cables', tab: 'cables' },
//   { name: 'Active Devices', tab: 'active-devices' },
//   { name: 'Passive Devices', tab: 'passive-devices' },
//   { name: 'Net Devices', tab: 'net-devices' },
//   { name: 'Net Devices', tab: 'net-devices2' },
//   { name: 'Net Devices', tab: 'net-devices3' },
//   { name: 'Net Devices', tab: 'net-devices4' },
//   { name: 'Net Devices', tab: 'net-devices5' },
//   { name: 'Net Devices', tab: 'net-devices6' },
//   { name: 'Servers and Computers', tab: 'servers-and-computers' },

// ];


interface ServiceNavigationProps { }

const ServiceNavigation: FC<ServiceNavigationProps> = () => {
  const ref = useRef<HTMLDivElement>(null);

  const { layoutStore } = useStores();

  const [tabs, setTabs] = useState([])



  useOutsideClickClose(ref, () => layoutStore.isOpenService && layoutStore.hideService());


  useMount(() => {
    CategoriesService.getCategories().then(data => {
      setTabs(data)
    });
  });




  return (
    <>
        <Categories tabs={tabs} />
    </>

  );
};

export default observer(ServiceNavigation);





