import styled from '@styles';

import { Layout } from 'antd';

const { Sider } = Layout;

// export const StyledServiceNavigation = styled.div<StyledServiceNavigationProps>`
export const StyledServiceNavigation = styled(Sider)`

/* 
        overflow: auto;
        height: 100vh; */
        /* position: fixed; */
        /* left: 0;
        top: 0;
        bottom: 0; */


        /* flex: 0 0 448px; */
        min-width: unset  !important;
        width: 100% !important;
        flex: unset !important;
        max-width: 448px !important;
        min-width: unset !important;
        padding: 40px 12%;
    

   

    .content {

      .categories-logo > img{

        max-width: 30px;
        max-height: 30px;
        height: 100%;
        width: 100%;
        display: inline-block;
      }
    
      .categories-menu{
        width: 100%;
        border-radius: 0 0 10px 10px;
        background-color:  rgba(223, 225, 225, 0.4);
        /* padding: 0 10px; */
      
    }
    .categories-menu-item{
        padding: 0 5px;
      }
    .categories-menu-item:not(:last-child){
        border-bottom: 1px solid rgba(25, 41, 59, 0.1);
      }

      .header {
        height:40px;
        background: #00909E;
        border-radius: 10px 10px 0px 0px;
        font-weight: bold;
        font-size: 21px;
        line-height: 40px;
        color: #fff;
        margin-bottom: 5px;
        text-align: start;
        padding: 0 20px;
        /* display: flex; */


        span {
          /* margin-left: 20px; */
        }

      }
      .categories-parent>.ant-collapse-item-active>{
        .ant-collapse-header{
          color: #00909E;
        }
      }
      .accordion-header-nested {
      

        .ant-collapse-content > .ant-collapse-content-box {
          padding-left: 15px;
          
        }
      
        .ant-collapse-header {
          padding: 10px 20px;
          }
        }

        .ant-collapse-item-active>
          .ant-collapse-header {

            font-weight: 600;
            

        }
        
      .categories-link{
        /* padding-left: 20px; */
        padding: 3px 0 3px 20px;
        display: list-item;

      }
      .categories-link:focus {
        font-weight: 600;
      }
      /* ul {
        // padding: 8px;

        border: 1px solid rgba(25, 41, 59, 0.1);
        border-radius: 0px 0px 10px 10px;
        margin-top: 5px;


        li {
          padding: 4px;
          height: 40px;
          background: rgba(223, 225, 225, 0.4); 
          border-top: 1px solid rgba(25, 41, 59, 0.1);
          display: flex;
          a{
            margin-left: 20px;
            :focus{
            color: #000;
            font-weight: bold;
            cursor: pointer;
}
          }
          a:active:not(:disabled):not(.loading) {
            cursor: pointer;
            font-weight: bold;
            color: #000;

          }
        }
      } */
    }
  /* } */
  .categories-dropdown{
  display: none;
}

  .small-header{
      display: none;
    }
    .categories-logo{
      width: 25px;
      height: 26px;
      background-color: #00909E;
      border-radius: 3px;
      margin-right:10px;
      padding: 2px;

    }

  @media (max-width: 768px) {
    padding: 15px;
    .categories-menu-large{

      display: none;
    }
.categories-dropdown{
  display: flex;
}
    .header{
      display: none;
    }
   
  }
`;
