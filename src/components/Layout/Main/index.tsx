import React, { FC } from 'react';
import { MainContent } from './styles';


interface MainProps {
  [extraProps: string]: any;
}


const Main: FC<MainProps> = ({ children, ...extraProps }) => (
  <MainContent className='site-main' {...extraProps}>



    {children}
  </MainContent>
);

export default Main;
