import styled from '@styles';

import {Layout} from 'antd';

const {Content } = Layout

// export const Main = styled.div<MainProps>`
export const MainContent = styled(Content)`

  /* width: calc(100% - 428px - 27px); */
  /* height: calc(100vh - 62px - 126px - 40px - 48px);  */
  /* overflow-y: scroll; */
  /* margin-top: 40px;
  margin-right: 27px; */
  margin: 40px 27px 20px 0px;
  /* float: right; */
   background: rgba(223, 225, 225, 0.4);

   @media (max-width: 768px) {
   margin: 20px 
  
  }
`;
