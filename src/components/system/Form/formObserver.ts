type Event = 'onChange' | 'onReset' | string;

export interface FormObserverInterface {
  dispatch(event: Event, newData: any): void;
  subscribe(event: Event, fieldName: string, func: any): string;
  unsubscribe(event: Event, fieldName: string): void;
  register(formData: any): void;
  unregister(): void;
}

export class FormObserver implements FormObserverInterface {
  events: {
    [key: string]: { subscribers: Map<string, any> };
  };

  formFields: { [kes: string]: { focused: boolean; value: any } };

  constructor(props: any) {
    this.events = {};

    props?.fields && this.register(props.fields);

    this.resetForm = this.resetForm.bind(this);
    this.formFields = {};
  }

  dispatch(event: Event, data: any) {
    if (!this.events[event]) {
      this.events[event] = {
        subscribers: new Map(),
      };
    }

    switch (event) {
      case 'onFocus':
        this.formFields[data.name] = {
          ...(this.formFields[data.name] || {}),
          focused: true,
        };
        break;
      case 'onBlur':
        this.formFields[data.name] = {
          ...(this.formFields[data.name] || {}),
          focused: false,
        };
        break;
      case 'onChange':
        this.formFields[data.name] = {
          ...(this.formFields[data.name] || {}),
          value: data.value,
        };
        break;
    }

    this.events[event].subscribers.get(data.name)?.(this.formFields);
  }

  subscribe(event: Event, fieldName: string, func: any) {
    if (!(event in this.events)) {
      this.events[event] = {
        subscribers: new Map(),
      };
    }
    this.events[event].subscribers.set(fieldName, func);
    return fieldName;
  }

  unsubscribe(event: Event, fieldName: string) {
    if (!(event in this.events)) return;
    this.events[event].subscribers.delete(fieldName);
  }

  register(formData: any) {
    this.formFields = Object.keys(formData).reduce(
      (acc, key) => ({ ...acc, [key]: formData[key].defaultValue }),
      {},
    );
  }

  unregister() {
    this.formFields = {};
    for (const event of Object.keys(this.events)) {
      this.events[event].subscribers.clear();
    }
  }

  resetForm() {
    this.events.onReset?.subscribers.forEach(func => func());
  }

  shakeInvalidInputs() {
    this.events.onFinishFailed?.subscribers.forEach(func => func());
  }

  getFormValues() {
    return Object.entries(this.formFields).reduce(
      (acc, [key, value]) => ({ ...acc, [key]: value.value }),
      {},
    );
  }
}

const createFormObserver = (props?: any): FormObserver => new FormObserver(props);

export default createFormObserver;
