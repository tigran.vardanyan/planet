import React, { createContext, FC, useMemo } from 'react';
import createFormObserver, { FormObserver } from './formObserver';

export type CustomFormContextType = {
  formObserver: FormObserver;
};

export const CustomFormContext = createContext<CustomFormContextType | null>(null);

export const CustomFormContextProvider: FC<CustomFormContextType> = ({
  children,
  formObserver,
}) => <CustomFormContext.Provider value={{ formObserver }}>{children}</CustomFormContext.Provider>;

export const useCustomFormObserver = () => useMemo(createFormObserver, []);
