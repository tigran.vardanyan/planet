import React, { FC, useContext, useEffect, useState } from 'react';
import { FormItemProps as AntFormItemProps } from 'antd';
import cn from 'classnames';

import { BRAND_CLASS_NAME } from '@styles/constants';
import { makeInputName } from '@planetkit/utils/makeInputName';
import { CustomFormContextType, CustomFormContext } from '@planetkit/Form/context';

import * as styles from './styles';

const cls = {
  focused: `${BRAND_CLASS_NAME}-form-item-focused`,
  field: `${BRAND_CLASS_NAME}-form-item-field`,
};

type FormItemProps = AntFormItemProps & {
  noObserve?: boolean;
};

const FormItem: FC<FormItemProps> = ({ children, className, noObserve = false, ...props }) => {
  const [classNames, setClassNames] = useState({});
  const formObserver = useContext<CustomFormContextType | null>(CustomFormContext)?.formObserver;

  useEffect(() => {
    if (noObserve) return () => null;

    const inputName = makeInputName(props.name as string) as string;
    
    formObserver?.subscribe('onFocus', inputName, (fieldsValue: any) => {
      
      setClassNames(classNames => ({
        ...classNames,
        [cls.focused]: fieldsValue[inputName]?.focused,
      }));
    });
    formObserver?.subscribe('onBlur', inputName, (fieldsValue: any) => {
      setClassNames(classNames => ({
        ...classNames,
        [cls.focused]: fieldsValue[inputName]?.focused,
      }));
    });
    formObserver?.subscribe('onChange', inputName, (fieldsValue: any) => {
      setClassNames(classNames => ({
        ...classNames,
        [cls.field]: fieldsValue[inputName]?.value,
      }));
    });

    return () => {
      formObserver?.unsubscribe('onBlur', inputName);
      formObserver?.unsubscribe('onFocus', inputName);
      formObserver?.unsubscribe('onChange', inputName);
    };
  }, [formObserver, props.name, noObserve]);

  return (
    <styles.FormItem className={cn({ className, ...classNames })} {...props}>
      {children}
    </styles.FormItem>
  );
};

export default FormItem;
