import React, { FC, useRef } from 'react';
import { Select as AntdSelect, SelectProps as AntdSelectProps } from 'antd';
import { BRAND_CLASS_NAME } from '@styles/constants';
import useInputEvents from '../hooks/useInputEvents';
import * as styles from './styles';

type SelectProps = AntdSelectProps<any> & {
  withContainer?: boolean;
};

const Select: FC<SelectProps> = props => {
  const { onBlurHandler, onFocusHandler, 
    onSelectChangeHandler
   } =
    useInputEvents<HTMLInputElement>(props);

  const selectContainer = useRef<HTMLDivElement>(null);

  return (
    <div ref={selectContainer}>
      <styles.Select
        {...props}
        onBlur={onBlurHandler}
        onFocus={onFocusHandler}
        //@ts-ignore
        onChange={onSelectChangeHandler}
        className={`${BRAND_CLASS_NAME}-select-styles`}
        getPopupContainer={
          props.withContainer ? () => selectContainer.current as HTMLDivElement : undefined
        }
      />
    </div>
  );
};

export default Object.assign(Select, {
  Option: AntdSelect.Option,
  OptGroup: AntdSelect.OptGroup,
  SECRET_COMBOBOX_MODE_DO_NOT_USE: AntdSelect.SECRET_COMBOBOX_MODE_DO_NOT_USE,
});
