import styled from '@styles';
import { Select as AntSelect } from 'antd';

export const Select = styled(AntSelect)`
  /**
   *  --- available classes for select
   * .ant-select-open 
   * .ant-select-single 
   * .ant-select-show-arrow 
   */
  &.ant-select {
    .ant-select-selector {
      height: 37px;
      padding: 0 14px;
      border: none !important;
      box-shadow: none !important;
      border-color: unset !important;
      background-color: unset !important;

      .ant-select-selection-search {
        .ant-select-selection-search-input {
        }

        .ant-select-selection-placeholder {
        }
      }
      .ant-select-selection-item {
        color: inherit;
        line-height: 40px;
        transition: all 0.2s ease;
      }
    }
    .ant-select-arrow {
      color: inherit;

      .anticon.anticon-down.ant-select-suffix {
        svg {
        }
      }
    }
  }
`;
