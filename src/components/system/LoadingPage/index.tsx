import React from 'react';

import Icon from '@planetkit/Icon';
import { StyledLoading } from './styles';

export const LoadingPage = () => (
  <StyledLoading>
    <Icon name='loading' />
  </StyledLoading>
);
