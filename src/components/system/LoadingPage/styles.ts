import styled, { keyframes } from '@styles';

const loopRotate = keyframes`
  0% {
    transform: translate(-50%, -50%) rotate(0);
  }
  100% {
    transform: translate(-50%, -50%) rotate(360deg);
  }
`;

export const StyledLoading = styled.div`
  height: 100%;
  display: grid;
  place-content: center;

  svg {
    width: 50px;
    height: 50px;

    animation: ${loopRotate} 0.8s linear infinite;
  }
`;
