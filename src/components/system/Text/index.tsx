import React, { FC, memo } from 'react';
import { Tag } from './styles';
import { TextProps, TextPropsWithConcreteTag } from './types';

const Text: FC<TextProps> = props => {
  const { children, size, color, tag, textRef, weight, ...textProps } = props;

  return (
    <Tag
      as={tag}
      sz={size}
      clr={color}
      ref={textRef}
      weight={weight}
      dangerouslySetInnerHTML={{ __html: String(children) }}
      {...textProps}
    />
  );
};

Text.defaultProps = {
  textRef: null,
  bold: false,
};

// Text with concrete tag
type TCT = TextPropsWithConcreteTag;

const H1: FC<TCT> = memo(({ size = '4xl', ...props }) => <Text {...props} tag='h1' size={size} />);
const H2: FC<TCT> = memo(({ size = '3xl', ...props }) => <Text {...props} tag='h2' size={size} />);
const H3: FC<TCT> = memo(({ size = '2xl', ...props }) => <Text {...props} tag='h3' size={size} />);
const H4: FC<TCT> = memo(({ size = 'xl', ...props }) => <Text {...props} tag='h4' size={size} />);
const H5: FC<TCT> = memo(({ size = 'l', ...props }) => <Text {...props} tag='h5' size={size} />);
const H6: FC<TCT> = memo(({ size = 'm', ...props }) => <Text {...props} tag='h6' size={size} />);
const P: FC<TCT> = memo(({ size = 'm', ...props }) => <Text {...props} tag='p' size={size} />);
const Span: FC<TCT> = memo(({ size = 'm', ...props }) => (
  <Text {...props} tag='span' size={size} />
));

export default Text;
export { H1, H2, H3, H4, H5, H6, P, Span };
