import { Ref } from 'react';
import { TextColor } from '@styles/theme/themes';
import { TextSizes, FontWeights } from './styles';

interface TextPropsBase extends React.HTMLAttributes<HTMLElement> {
  color?: TextColor;
  weight?: FontWeights;
  textRef?: Ref<HTMLDivElement>;
  [key: string]: any;
}

export interface TextProps extends TextPropsBase {
  size: TextSizes;
  tag: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6' | 'p' | 'span' | 'label';
}

export interface TextPropsWithConcreteTag extends TextPropsBase {
  size?: TextSizes;
}
