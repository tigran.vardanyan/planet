import React, { FC } from 'react';
import './styles.css'
// import 'antd/dist/antd.css';

const values = ['----', 'low to high', 'high to low'];


const Sort: FC<any> = ({ cards, setCards }) => {


  const handleProvinceChange = (value: string) => {





    const sortedCards = cards.sort((a: any, b: any) => {

      // switch (value) {
      //   case values[1]:
      //     return a.price - b.price;
      //   case value[2]:
      //     return b.price - a.price;
      //   default:
      //     return a.price - a.price;
      // }



      if (value === values[1]) {

        return a.price - b.price;
      }
      else {

        return b.price - a.price;
      }



    });
    setCards((prev:any)=> [...prev],[...sortedCards])


  };


  return (
    <>
      <select defaultValue={values[0]} onChange={(e) => handleProvinceChange(e.target.value)} className='sort'>
        {values.map(value => (
          <option key={value}>{value}</option>
        ))}
      </select>
    </>
  );
};

export default Sort;