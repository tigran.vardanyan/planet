import { BRAND_CLASS_NAME } from '@styles/constants';
import React, { Slider } from 'antd';
import { FC } from 'react';
import * as styles from './styles';

interface RangeProps {
  min: number;
  max: number;
  tooltipVisible?: boolean;
  defaultValue?: [number, number];
  onChange?: (value: [number, number]) => void;
}

const Range: FC<RangeProps> = ({ ...props }) => {
  return (
    <styles.Range>
      <Slider className={`${BRAND_CLASS_NAME}-slider`} {...props} range />
    </styles.Range>
  );
};

export default Range;
