import styled from '@styles';
import { BRAND_CLASS_NAME } from '@styles/constants';

export const Range = styled.div`
  .${BRAND_CLASS_NAME}-slider {
    .ant-slider-track {
      background-color: ${p => p.theme.range.track};
    }

    .ant-slider-rail {
      background-color: ${p => p.theme.range.rail};
    }

    .ant-slider-handle {
      background-color: ${p => p.theme.range.circle};
      border: none;
    }
  }
`;
