// export type { ButtonType } from '@styles/theme/themes';
export type IconName = 'plus' | 'dropdown';
// ant design types
export type Size = 'large' | 'middle' | 'small';
export type AntButtonType = 'default' | 'text' | 'link' | 'ghost' | 'primary' | 'dashed';


// planet types


export type ButtonType = 'text' | 'primary';
