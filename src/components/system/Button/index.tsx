import React, { FC, memo } from 'react';

import { ButtonProps as AntButtonProps } from 'antd';
import cn from 'classnames';

import Icon, { IconName } from '@planetkit/Icon';
import { BRAND_CLASS_NAME } from '@styles/constants';

import { ButtonType } from './types';
import * as styles from './styles';

type ButtonProps = Omit<AntButtonProps, 'type' | 'icon'> & {
  type: ButtonType;
  loading?: boolean;
  icon?: IconName | React.ReactNode;
  [x: string]: any;
};

const Button: FC<ButtonProps> = ({ type, size, icon, loading, className = '', ...props }) => {
  const loadingIcons: Pick<any, ButtonType> = {
    primary: 'loading',
    // ghost: 'loadingDark',
    // default: 'loadingDark',
    text: 'loading',
    // delete: 'deleteOpen',
  };
  

  const CustomIcon = ({ name }: any) => (
    <span className='anticon'>
      <Icon name={name} />
    </span>
  );

  const LoadingIcon = ({ type }: { type: ButtonType }) => (
    <span className='ant-btn-loading-icon'>
      <CustomIcon name={loadingIcons[type]} />
    </span>
  );

  let IconComponent: any;
  if (React.isValidElement(icon)) {
    IconComponent = icon;
  } else {
    // eslint-disable-next-line no-nested-ternary
    IconComponent = loading ? (
      <LoadingIcon type={type} />
    ) : icon ? (
      <CustomIcon name={icon} />
    ) : null;
  }

  return (
    <styles.Button
      size={size}
      type={ type}
      icon={IconComponent}
      className={cn({
        [className]: true,
        [`${BRAND_CLASS_NAME}-${type}-${className}`] : true,
        [`${BRAND_CLASS_NAME}-${type}-${className}`] : true,
        [`${BRAND_CLASS_NAME}-btn`]: true,
        [`${BRAND_CLASS_NAME}-btn-${type}`]: true,
        [`${BRAND_CLASS_NAME}-btn-loading`]: loading,
      })}
      {...props}
    />
  );
};

export default memo(Button);
