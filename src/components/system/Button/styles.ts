import styled, { breakpoints, css } from '@styles';
import { BRAND_CLASS_NAME } from '@styles/constants';
import { Button as AntButton } from 'antd';
// import ButtonType from './buttonTypes';

const sizes = {
  small: css`
    height: 48px;
    padding: 0 16px;
    font-size: 12px;
  `,
  middle: css`
    height: 48px;
    padding: 0 18px;
    font-size: 20px;
  `,
  large: css`
    height: 48px;
    padding: 0 21px;
    font-size: 20px;
  `,
};



export const Button = styled(AntButton)`
/* 
  &.buy {
    width: 195px;
    height:48px;
    border-radius: 50px;
    
  }
  &.sign-in {
    width: 126px;
    height: 45px;
    border-radius: 50px;

  }
  &.message {
    width: 179px;
    height: 37px;
    border-radius: 50px;

  }
  &.add-card {
    width: 243px;
    height: 30px;
    border-radius: 50px;
    font-size: 13px !important; 
    line-height: 15px;
  }
  &.search {
    width: 110px;
    height: 45px;
    border-radius: 0px 50px 50px 0px;
    position: relative;
    top: 5px;
  } */
  &.white {
   color: #00909E !important;
   background-color: white !important;
   border: 1px solid #00909E !important ;

  }



  &.${BRAND_CLASS_NAME}-btn {
    display: inline-flex;
    align-items: center;

    // ---- wrapper for ant icon when loading
    .ant-btn-loading-icon {
      order: 1;
      display: flex;
      align-items: center;
    }

    .anticon {
      order: 1;
      padding-right: unset;
      padding-left: unset;
      display: flex;
      align-items: center;

      + span {
        margin: unset;
      }
    }

    /* :hover:not(:disabled):not(.loading):not(.menu-item) {
      cursor: pointer;
      opacity: 0.7
    }

    :active:not(:disabled):not(.loading):not(.menu-item) {
      cursor: pointer;
      opacity: 0.7
    } */

    :disabled {
      cursor: no-drop;
    }

    &-text {
      color: ${p => p.theme.btn?.text.text.default};
      background: unset;
      border: unset;
      box-shadow: unset;
      font-size: 18px;
      font-weight: 700;
      padding: 20px;
      // text-decoration: underline;
      // font-weight: bold;
      // font-size: 18px;
      // line-height: 21px;

      :hover:not(:disabled):not(.loading) {
        background: unset;
        // text-decoration: underline;
      }
      :focus:not(:disabled):not(.loading) {
        box-shadow: unset !important;
      }
      :active:not(:disabled):not(.loading) {
        box-shadow: unset !important;
        
      }
      :disabled {
        color: #000;
        opacity: 0.6
      }
      :focus{

        color: #00909E;
        text-decoration-line: underline;
        opacity: 1;
        cursor: pointer;

      }
    }
   

    &-primary {
      color: ${p => p.theme.btn?.primary.text.default || '#fff'};
      background: ${p => p.theme.btn?.primary.bg.default || '#00909E'};
      border: none;
      font-weight: normal;
      font-size: 16px;
      box-shadow: 2px 2px 2px rgba(25, 41, 59, 0.2);
      border-radius: 50px;

      >span, svg {
        width: 100%;
      }
   
      :hover:not(:disabled):not(.loading) {
        background: ${p => p.theme.btn?.primary.bg.hover};
        box-shadow: 2px 2px 2px rgba(25, 41, 59, 0.2);
      }
      }
      :focus:not(:disabled):not(.loading) {
        background: ${p => p.theme.btn?.primary.bg.hover};
        box-shadow: 2px 2px 2px rgba(25, 41, 59, 0.2);
      }
      :active:not(:disabled):not(.loading) {
        background: ${p => p.theme.btn?.primary.bg.pressed};
        box-shadow: inset 0px 4px 4px rgba(25, 41, 59, 0.3);
      }
      :disabled {
        background: ${p => p.theme.btn?.primary.bg.disabled};
        color: ${p => p.theme.btn?.primary.text.disabled};
      }
    }

    /* &-default {
      color: ${p => p.theme.btn?.default.text.default};
      background: unset;
      border: unset;
      box-shadow: unset;
      text-decoration: underline;
      font-weight: bold;

      :hover:not(:disabled):not(.loading) {
        background: unset;
        text-decoration: underline;
      }
      :active:not(:disabled):not(.loading) {
        background: ${p => p.theme.btn?.default.bg.pressed};
      }
      :disabled {
        background: ${p => p.theme.btn?.default.bg.disabled};
      }
    } */

    /* &-ghost {
      color: ${p => p.theme.btn?.ghost.text.default};
      background: ${p => p.theme.btn?.ghost.bg.default};
      border: none;

      &.loading {
        background: ${p => p.theme.btn?.ghost.bg.loading};
      }

      :hover:not(:disabled):not(.loading) {
        background: ${p => p.theme.btn?.ghost.bg.hover};
      }
      :active:not(:disabled):not(.loading) {
        background: ${p => p.theme.btn?.ghost.bg.pressed};
      }
      :disabled {
        background: ${p => p.theme.btn?.ghost.bg.disabled};
      }
    } */

    /* &-delete {
      color: ${p => p.theme.btn?.delete.text.default};
      background: ${p => p.theme.btn?.delete.bg.default};
      border: none;

      :hover:not(:disabled):not(.loading),
      :focus:not(:disabled):not(.loading) {
        background: ${p => p.theme.btn?.delete.bg.hover};
      }
      :active:not(:disabled):not(.loading) {
        background: ${p => p.theme.btn?.delete.bg.pressed};
      }
      :disabled {
        background: ${p => p.theme.btn?.delete.bg.disabled};
        color: ${p => p.theme.btn?.delete.text.disabled};
      }
    } */

    /* ${breakpoints.down('xs')} {
      ${sizes.large}
    }

    ${breakpoints.between('xs', 'lg')} {
      ${sizes.small}
    }

    ${breakpoints.between('lg', 'xl')} {
      ${sizes.middle}
    }

    ${breakpoints.up('xl')} {
      ${sizes.middle}
    } */

    &&& {
      ${p => p.size && sizes[p.size]}
    }
  /* } */

  /* &.${BRAND_CLASS_NAME}-text {
    &-small-icon{
      font-size: 14px;
      .anticon {
        margin-left: -50%;
      }
    }

    &-big {
      line-height: 23px ;
      font-size: 20px ;
      .anticon {
        padding-left:8px !important;
      }
    }
    &-small {
      font-size: 14px ;
      line-height: 16px ;

    }
    &-standard {
      font-weight: bold;
      font-size: 18px;
      line-height: 21px;
      text-decoration-line: underline;
    }
    &-menu-item {
      font-weight: bold;
      font-size: 18px;
      line-height: 21px;
      text-decoration-line: none;
    }
  
    &-menu-item :hover:not(:disabled):not(.loading) {
      color: #00909E;
      text-decoration-line: underline;
      opacity: 1;
      cursor: pointer;

    }

    &-menu-item :active:not(:disabled):not(.loading) {
      cursor: pointer;
      color: #00909E;

    }


    &-x-small {
      font-size: 12px;
      line-height: 14px;
      text-decoration-line: underline;
    }
  }   */


  /* &.${BRAND_CLASS_NAME}-btn-loading {
    .ant-btn-loading-icon {
      position: absolute;
      left: 50%;
      transform: translateX(-50%);

      .anticon {
        padding: unset;
      }

      & + span {
        opacity: 0;
      }
    }
  } */
`
