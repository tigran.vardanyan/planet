import { Button as BaseButton, ButtonProps } from 'antd';
import { ButtonType as BaseButtonType } from 'antd/lib/button';
import React from 'react';

type ButtonType = BaseButtonType | 'submit';

export default function Button(props: Omit<ButtonProps, 'type'> & React.RefAttributes<HTMLElement> & { type?: ButtonType }) {
  return <BaseButton {...(props as any)} />
};