import React, { FC, forwardRef, useRef } from 'react';
import { Input } from 'antd';
import mergeRefs from '@utils/types/mergeRefs';
import { BRAND_CLASS_NAME } from '@styles/constants';

import * as styles from './styles';
import useInputEvents from '../../hooks/useInputEvents';

type TextAreaProps = React.ComponentProps<typeof Input.TextArea>;

const TextArea: FC<TextAreaProps> = forwardRef((props, ref) => {
  const localRef = useRef<HTMLTextAreaElement>(null);
  const { onBlurHandler, onFocusHandler, onInputHandler } = useInputEvents<HTMLTextAreaElement>(
    props,
  );

  return (
    <styles.TextArea
      {...props}
      autoCorrect='off'
      autoComplete='off'
      onBlur={onBlurHandler}
      onFocus={onFocusHandler}
      onInput={onInputHandler}
      ref={mergeRefs(ref, localRef)}
      className={`${BRAND_CLASS_NAME}-input-styles`}
    />
  );
});

export default TextArea;
