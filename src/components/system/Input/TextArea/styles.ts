import { Input } from 'antd';
import styled from '@styles';

export const TextArea = styled(Input.TextArea)`
  &&&&&& {
    padding-top: 4px;
    height: 140px;
  }
`;
