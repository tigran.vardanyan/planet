import React, { FC, forwardRef, useRef } from 'react';
import {
  Input as AntInput,
  InputProps as AntInputProps,
  InputNumberProps as AntInputNumberProps,
} from 'antd';
import cn from 'classnames';

import mergeRefs from '@utils/types/mergeRefs';
import { BRAND_CLASS_NAME } from '@styles/constants';

import TextArea from './TextArea';
import * as styles from './styles';
import useInputEvents from '../hooks/useInputEvents';

export type InputProps = AntInputProps;

const Input: FC<InputProps> = forwardRef((props, ref) => {
  // --- localRef.current => { state: { focused: false, prevValue: undefined, value: undefined} }
  const localRef = useRef<AntInput>(null);

  const { onBlurHandler, onFocusHandler, onInputHandler } = useInputEvents<HTMLInputElement>(props);

  return (
    <styles.Input
      {...props}
      autoCorrect='off'
      autoComplete='off'
      onBlur={onBlurHandler}
      onFocus={onFocusHandler}
      onInput={onInputHandler}
      ref={mergeRefs(ref, localRef)}
      className={cn(`${BRAND_CLASS_NAME}-input-styles`, props.className)}
    />
  );
});

const InputNumber: FC<AntInputNumberProps> = forwardRef((props, ref) => {
  // --- localRef.current => { state: { focused: false, prevValue: undefined, value: undefined} }
  const localRef = useRef<any>(null);

  const { onBlurHandler, onFocusHandler, onInputHandler } = useInputEvents<HTMLInputElement>(props);

  return (
    <styles.InputNumber
      {...props}
      autoCorrect='off'
      autoComplete='off'
      onBlur={onBlurHandler}
      onFocus={onFocusHandler}
      onInput={(text: string) => {
        onInputHandler({ target: { value: text } } as React.ChangeEvent<HTMLInputElement>);
      }}
      ref={mergeRefs(ref, localRef)}
      className={cn(`${BRAND_CLASS_NAME}-number-input-styles`, props.className)}
    />
  );
});

export { InputNumber };

export default Object.assign(Input, {
  TextArea,
  Group: AntInput.Group,
  Search: AntInput.Search,
  Password: AntInput.Password,
});
