import styled from '@styles';
import { Card as AntCard } from 'antd';
// import CardType from './CardTypes';


export const Card = styled(AntCard)`

  max-width: 270px;
  min-width: 255px;
  width: 100%;
  /* height: 340px; */
  max-height: 370px;
  background: #FFFFFF;
  box-shadow: 1px 2px 2px rgba(25, 41, 59, 0.15);
  z-index:999;
  display: inline-block;
  /* padding: 15px; */

  .cardRout {
    cursor: pointer;
    padding-top: 20px;
  }
  .cardRout>img {
    height: 100%;
    width: 100%;
    object-fit: cover;
  }

  .ant-card-cover {
    /* display: flex;
    justify-content: center; */
    /* padding-top: 10px;
    padding-bottom: 10px; */
    height:30%;
    img {
      // max-width: 192px;
      /* width: 80%; */
      max-height: 140px;
      height: 80%;
    }
  }
  
  li {
    width: unset !important;
  }

  .card-content {
    margin-top: 10px;
    height:70%;
    
    .title{
      font-size: 18px;
      height:20%;
      /* line-height: 21px; */
/*      
      text-overflow: ellipsis;
      overflow: hidden;
      white-space: nowrap; */
  }
  .title span{
    display: inline-block;
    width: 180px;
    white-space: nowrap;
    overflow: hidden !important;
    text-overflow: ellipsis;
  }

  .price{
    height:10%;
  }

  .count-content{
  
    padding: 0 5px 10px 10px;
    height:10%;

    button {
      padding: 0;
      height: 32px;

    }
   
  }

 

  .actions {
    line-height: 23px;
  
  }
  .count{
    font-size: 16px;
    margin: 13px 10px 10px 0px;
    position: relative;
    right: 7px;
  }
 
   .add-card-btn {
    /* max-width: 243px; */
    width: 100%;
    height: 32px;
    border-radius: 50px;
    font-size: 13px !important; 
    line-height: 15px;
  }
  .small-icon{
      font-size: 14px;
      .anticon {
        margin-left: -50%;
        line-height: 23px;
      }
    }

    .add-card-content{
      top: 7px;
    }
  
}


  `;
