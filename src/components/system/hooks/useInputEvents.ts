import { useCallback, useContext, useEffect, useMemo } from 'react';
import { SelectValue } from 'antd/lib/select';
import { CustomFormContext, CustomFormContextType } from '@planetkit/Form/context';

const useInputEvents = <T extends { value: any }>(props: any) => {
  const formObserver = useContext<CustomFormContextType | null>(CustomFormContext)?.formObserver;

  const name = useMemo(() => {
    // --- if field is 'ListField' and starts with list name than remove 'list name'
    if (props.id?.split('_')[2]) {
      const [, ...rest] = props.id?.split('_');
      return rest.join('_');
    }

    return props.id;
  }, [props.id]);

  useEffect(() => {
    if (props.value) {
      setTimeout(() => {
        formObserver?.dispatch('onChange', {
          name,
          value: props.value,
        });
      }, 0);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    props.value ??
      formObserver?.dispatch('onChange', {
        name,
        value: props.value,
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.value, formObserver?.events.onChange?.subscribers.size]);

  const onSelectChangeHandler = useCallback(
    (value: SelectValue, option: any) => {
      formObserver?.dispatch('onChange', {
        value,
        name,
      });
      props.onChange?.(value, option);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [formObserver, name, props.onChange],
  );

  const onInputHandler = useCallback(
    (event: React.ChangeEvent<T>) => {
      formObserver?.dispatch('onChange', {
        name,
        value: event.target.value,
      });
      props.onInput?.(event);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [formObserver, name, props.onInput],
  );

  const onFocusHandler = useCallback(
    (event: React.FocusEvent<T>) => {
      formObserver?.dispatch('onFocus', { name });
      props.onFocus?.(event);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [formObserver, name, props.onFocus],
  );

  const onBlurHandler = useCallback(
    (event: React.FocusEvent<T>) => {
      formObserver?.dispatch('onBlur', { name });
      props.onBlur?.(event);
    },
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [formObserver, name, props.onBlur],
  );

  return {
    onBlurHandler,
    onFocusHandler,
    onInputHandler,
    onSelectChangeHandler,
  };
};

export default useInputEvents;
