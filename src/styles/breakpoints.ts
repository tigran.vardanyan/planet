const unit = 'px';
const step = 2;

export const values = {
  min: 0,
  fab: 480,
  xs: 768,
  sm: 960,
  md: 1024,
  lg: 1440,
  xl: 1760,
  xxl: 1920,
};

type Bp = keyof typeof values;

export const keys = Object.keys(values) as Bp[];

export function up(key: Bp | number) {
  const value = typeof values[key as Bp] === 'number' ? values[key as Bp] : key;
  return `@media (min-width:${value}${unit})`;
}

export function down(key: Bp | number) {
  const endIndex: number = keys.indexOf(key as Bp);
  const upperBound: number = values[keys[endIndex] as Bp];

  if (endIndex === keys.length) {
    // md down applies to all sizes
    return up('xs');
  }

  const value = typeof upperBound === 'number' && endIndex > 0 ? upperBound : (key as number);
  return `@media (max-width:${value - step / 100}${unit})`;
}

export function between(start: Bp, end: Bp) {
  const endIndex = keys.indexOf(end);

  if (endIndex === keys.length) {
    return up(start);
  }

  return (
    `@media (min-width:${values[start]}${unit}) and ` +
    `(max-width:${values[keys[endIndex]] - step / 100}${unit})`
  );
}

export function only(key: any) {
  return between(key, key);
}

export function width(key: Bp) {
  return values[key];
}
