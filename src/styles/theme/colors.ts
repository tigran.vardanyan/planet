export const TRANSPARENT = '#00000000';
// Neutrals
export const N10 = '#FAFAFA';
export const N20 = '#F5F5F5';
export const N30 = '#F3F6F6';
export const N40 = '#F1F6F8';
export const N50 = '#E5EFF1';
export const N60 = '#E5E6E6';
export const N70 = '#E4E9EB';
export const N80 = '#DFE4E6';
export const N90 = '#DBE8EC';
export const N100 = '#D6EDF1';
export const N110 = '#CAE1E8';
export const N120 = '#EFF3FF';
export const N130 = '#DEE6FF';
export const N140 = '#D4D4D4';
export const N150 = '#C8CFD1';
export const N160 = '#B2B2B2';
export const N170 = '#848E92';
export const N180 = '#6C7374';
export const N190 = '#566070';
export const N200 = '#4B5565';
export const N210 = '#43474D';
export const N220 = '#353846';
export const N230 = '#282A2E';
export const N240 = '#202226';
export const N250 = '#1D1E23';
export const N260 = '#171718';
export const N270 = '#0F0F13';
// Neutrals with alpha channel
export const N0A40 = 'rgba(255, 255, 255, 0.4)';
export const N0A03 = 'rgba(255, 255, 255, 0.03)';
export const N80A40 = 'rgba(223, 228, 230, 0.4)';
export const N140A60 = 'rgba(212, 212, 212, 0.6)';
export const N140A40 = 'rgba(212, 212, 212, 0.4)';
export const N150A80 = 'rgba(200, 207, 209, 0.8)';
export const N150A60 = 'rgba(200, 207, 209, 0.6)';
export const N150A50 = 'rgba(200, 207, 209, 0.5)';
export const N160A40 = 'rgba(178, 178, 178, 0.4)';
export const N180A10 = 'rgba(108, 115, 116, 0.1)';
export const N190A60 = 'rgba(86, 96, 112, 0.6)';
export const N190A05 = 'rgba(86, 96, 112, 0.05)';
export const N190A03 = 'rgba(86, 96, 112, 0.03)';
export const N220A50 = 'rgba(53, 56, 70, 0.5)';
export const N200A60 = 'rgba(75, 85, 101, 0.6)';
export const N200A50 = 'rgba(75, 85, 101, 0.5)';
export const NA50 = 'rgba(95, 103, 106, 0.5)';
export const NA70 = 'rgba(62, 66, 77, 0.7)';
// Blues
export const B10 = '#0AB1EE';
export const B20 = '#09ACDD';
export const B30 = '#00A4D6';
export const B40 = '#0A9AC5';
export const B50 = '#1991B6';
export const B60 = '#097BAF';
// Blues with alpha channel
export const B20A12 = 'rgba(9; 172; 221; 0.12)';
export const B20A05 = 'rgba(9, 172, 221, 0.05)';
// Greens
export const G10 = '#0BCFBF';
export const G20 = '#0AC5B5';
export const G30 = '#09B3A5';
// Reds
export const R10 = '#FAA6A7';
export const R30 = '#F23E41';
export const R40 = '#ED2326';
// Reds with alpha channel
export const R40A40 = 'rgba(185, 50, 50, 0.4)';

// NEW COLORS --------------------------------------------->

// Orange
export const O10 = '#F89824';

// Yellow
export const Y10 = '#F7DD42';
export const Y11 = 'rgba(247, 221, 66, 0.6);';

// White
export const N0 = '#FFFFFF';
export const N1 = 'rgba(255, 255, 255, 0.8)';
export const N2 = 'rgba(255, 255, 255, 0.5)';
export const N3 = 'rgba(255, 255, 255, 0.6)';

export const W1 = '#F9F9F9';

export const D0 = 'rgba(255, 255, 255, 0.2);';

// Gray
export const D10 = '#75777A';
export const D11 = 'rgba(117, 119, 122, 0.75)';

export const D20 = '#939598';
export const D21 = 'rgba(147, 149, 152, 0.5)';

export const D30 = '#C1C1C1';
export const D32 = 'rgba(193, 193, 193, 0.7)';
export const D33 = 'rgba(193, 193, 193, 0.35)';
export const D31 = 'rgba(193, 193, 193, 0.2)';

export const D40 = '#DDDDDD';
export const D41 = 'rgba(221, 221, 221, 0.8)';

export const D50 = '#2d2d2d';
export const D51 = 'rgba(45, 45, 45, 0.8)';
export const D52 = 'rgba(45, 45, 45, 0.85)';

export const D60 = '#353535';

// Red
export const R20 = '#E75B5D';

// Black
export const D1 = '#000000';
export const D2 = 'rgba(0, 0, 0, 0.9)';
export const D3 = 'rgba(0, 0, 0, 0.8)';
export const D4 = 'rgba(0, 0, 0, 0.7)';
export const D5 = 'rgba(0, 0, 0, 0.6)';


export const G1 = '#149b9e';
