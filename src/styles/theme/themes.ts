import { themed } from '@styles/utils/themed';

// import { ReactComponent as DarkThemeLogo } from '@assets/images/icons/dark_logo.svg';
// import { ReactComponent as DarkThemeDelete } from '@assets/images/icons/dark_delete.svg';
// import { ReactComponent as DarkThemeEdit } from '@assets/images/icons/dark_edit.svg';

// import { ReactComponent as LightThemeLogo } from '@assets/images/icons/light_logo.svg';
// import { ReactComponent as LightThemeDelete } from '@assets/images/icons/light_delete.svg';
// import { ReactComponent as LightThemeEdit } from '@assets/images/icons/light_edit.svg';

// import { ReactComponent as LightMapping } from '@assets/images/icons/light_mapping.svg';
// import { ReactComponent as LightLibrary } from '@assets/images/icons/light_library.svg';
// import { ReactComponent as LightWarehouse } from '@assets/images/icons/light_warehouse.svg';
// import { ReactComponent as LightSettings } from '@assets/images/icons/light_settings.svg';
// import { ReactComponent as LightTicket } from '@assets/images/icons/light_ticketing.svg';
// import { ReactComponent as LightDashboard } from '@assets/images/icons/light_dashboard.svg';

// import { ReactComponent as DarkMapping } from '@assets/images/icons/dark_mapping.svg';
// import { ReactComponent as DarkLibrary } from '@assets/images/icons/dark_library.svg';
// import { ReactComponent as DarkWarehouse } from '@assets/images/icons/dark_warehouse.svg';
// import { ReactComponent as DarkSettings } from '@assets/images/icons/dark_settings.svg';
// import { ReactComponent as DarkTicket } from '@assets/images/icons/dark_ticketing.svg';
// import { ReactComponent as DarkDashboard } from '@assets/images/icons/dark_dashboard.svg';

import * as colors from './colors';

const boxShadow = {
  shadow1: { light: '2px 2px 4px 0px #D9E0E2', dark: '3px 3px 3px rgb(255 255 255 / 5%)' },
  shadow2: { light: '0px 0px 2px #C5CACC', dark: '0px 0px 2px #C5CACC' },
  shadow3: {
    light: `2px 2px 4px ${colors.N140A60}`,
    dark: `2px 2px 4px ${colors.N140A60}`,
  },
  shadow4: {
    light: '-5px -2px 7px rgba(255, 255, 255, 0.74), 2px 2px 7px rgba(0, 0, 0, 0.2)',
    dark: '-5px -2px 7px rgba(255, 255, 255, 0.2), 2px 2px 7px rgba(0, 0, 0, 0.6)',
  },
  shadow5: {
    light: '2px 2px 4px rgba(89, 89, 89, 0.2)',
    dark: '5px 5px 4px rgba(78, 78, 78, 0.2)',
  },
  shadow6: {
    light: '1px 0px 4px rgba(0, 0, 0, 0.25);',
    dark: '1px 0px 4px rgba(0, 0, 0, 0.25);',
  },
  shadow7: {
    light: '3px 4px 6px rgba(0, 0, 0, 0.1);',
    dark: '3px 4px 6px rgba(0, 0, 0, 0.1);',
  },
};

export const themedColor = {
  background: {
    primary: themed({ light: colors.N0, dark: colors.N230 }),
    forth: themed({ light: colors.N0, dark: colors.N150 }),
  },
  table: {
    rowText: themed({ light: colors.D1, dark: colors.N140 }),
    rowShadow: themed({
      light: `drop-shadow(1px 4px 5px ${colors.N70})`,
      dark: `drop-shadow(0px 1px 3px ${colors.N210})`,
    }),
    rowFilters: themed({ light: colors.N30, dark: colors.N210 }),
    background: themed({ light: colors.N10, dark: colors.N240 }),
    action: {
      hover: themed({ light: colors.N80A40, dark: colors.N80A40 }),
      hoverItem: themed({ light: colors.N200, dark: colors.N0 }),
    },
  },
  modalMask: themed({ light: colors.NA50, dark: colors.NA70 }),
};

// TODO needs to fix HOVO delete all previous colors

export const lightTheme = {
  white: colors.N0,

  // icons: {
  //   Logo: LightThemeLogo,
  //   Delete: LightThemeDelete,
  //   Edit: LightThemeEdit,
  //   Mapping: LightMapping,
  //   Library: LightLibrary,
  //   Warehouse: LightWarehouse,
  //   Settings: LightSettings,
  //   Ticket: LightTicket,
  //   Dashboard: LightDashboard,

  //   boxShadow: { default: boxShadow.shadow5.light },
  // },

  text: {
    default: colors.N250,
    primary: colors.D1,
    secondary: colors.O10,
    error: colors.R30,
    tertiary: colors.D10,
    white: colors.N0,
  },

  lines: {
    default: colors.D32,
  },

  scroll: {
    primary: colors.D32,
    secondary: colors.D33,
  },

  range: {
    rail: colors.D33,
    track: colors.O10,
    circle: colors.O10,
  },

  input: {
    bg: {
      error: colors.N0,
      hover: colors.N40,
      default: colors.N0,
      disabled: colors.N0,
      pressed: colors.TRANSPARENT,
    },
    text: {
      error: colors.R30,
      default: colors.D1,
      placeholder: colors.D3,
      disabled: colors.N200A60,
    },
    border: {
      hover: colors.D1,
      error: colors.R30,
      pressed: colors.D1,
      default: colors.D2,
      disabled: colors.N150A60,
      highlighted: colors.O10,
    },
    boxShadow: {},
  },
  btn: {
    primary: {
      bg: {
        default: colors.D1,
        hover: colors.D2,
        pressed: colors.D3,
        disabled: colors.D4,
      },
      text: { default: colors.N0, disabled: 'black' },
      boxShadow: { default: boxShadow.shadow4.light },
    },


    text: {
      bg: {
        default: colors.D1,
        hover: colors.D2,
        pressed: colors.D3,
        disabled: colors.D4,
      },
      text: { default: colors.N0, disabled: 'black' },
      boxShadow: { default: boxShadow.shadow4.light },
    },


    default: {
      bg: {
        default: colors.N0,
        hover: colors.N120,
        pressed: colors.N130,
        disabled: colors.N80,
      },
      text: { default: colors.D1, disabled: colors.N190 },
      border: {
        default: colors.N160,
        hover: colors.N160,
        pressed: colors.N160,
        disabled: colors.N160,
      },
    },
    ghost: {
      bg: {
        default: colors.TRANSPARENT,
        hover: colors.N50,
        pressed: colors.N110,
        disabled: colors.N70,
        loading: colors.N90,
      },
      text: {
        default: colors.N190,
        hover: colors.N190,
        pressed: colors.N190,
        disabled: colors.N190,
        loading: colors.N190,
      },
    },

    
    delete: {
      bg: {
        default: colors.R20,
        hover: colors.R30,
        pressed: colors.R40,
        disabled: colors.R10,
      },
      text: { default: colors.N0, disabled: colors.N0 },
      boxShadow: { hover: boxShadow.shadow3.dark },
    },
  },
  dropdown: {
    bg: {
      hover: colors.B20A05,
      pressed: colors.B20A12,
    },
  },
  bg: { default: colors.N0, primary: colors.B10, multiSelect: colors.N60 },
  border: { default: colors.B20, tertiary: colors.G10 },
  checkbox: {
    border: {
      active: colors.O10,
      inActive: colors.D4,
      disabled: colors.N190A05,
    },
    bg: { active: colors.N0, inActive: colors.N0, disabled: colors.N190A03 },
  },
  radio: {
    border: { active: colors.O10, noActive: colors.D30 },
    bg: { active: colors.O10 },
  },
  calendar: {
    text: {
      week: colors.N220A50,
      disabled: colors.N200A50,
      default: colors.N200,
    },
    bg: {
      hover: colors.N10,
      active: colors.G10,
      pressed: colors.N100,
    },
  },
  background: {
    primary: colors.N0,
    secondary: colors.N0,
    third: colors.D31,
    fourth: colors.D1,
    fifth: colors.D1,
    sixth: colors.W1,
  },
  boxShadow: {
    primary: boxShadow.shadow6.light,
    secondary: boxShadow.shadow7.light,
  },
  mode: 'light' as ThemeModeType,
};

export const darkTheme: typeof lightTheme = {
  ...lightTheme,

  // icons: {
  //   Logo: DarkThemeLogo,
  //   Delete: DarkThemeDelete,
  //   Edit: DarkThemeEdit,
  //   Mapping: DarkMapping,
  //   Library: DarkLibrary,
  //   Warehouse: DarkWarehouse,
  //   Settings: DarkSettings,
  //   Ticket: DarkTicket,
  //   Dashboard: DarkDashboard,

  //   boxShadow: { default: boxShadow.shadow5.dark },
  // },

  text: {
    default: colors.N0,
    secondary: colors.D1,
    primary: colors.N0,
    error: colors.R30,
    tertiary: colors.B20,
    white: colors.N0,
  },

  lines: {
    default: colors.N0,
  },

  range: {
    rail: colors.D30,
    track: colors.O10,
    circle: colors.O10,
  },

  input: {
    bg: {
      default: colors.D50,
      hover: colors.N0A03,
      pressed: colors.TRANSPARENT,
      disabled: colors.N180A10,
      error: colors.TRANSPARENT,
    },
    text: {
      default: colors.N0,
      error: colors.R30,
      disabled: colors.N140A60,
      placeholder: colors.N80,
    },
    border: {
      hover: colors.D40,
      error: colors.R40,
      pressed: colors.D32,
      default: colors.D40,
      disabled: colors.N150A60,
      highlighted: colors.O10,
    },
    boxShadow: {},
  },
  btn: {
    primary: {
      bg: {
        default: colors.N0,
        hover: colors.N0,
        pressed: colors.N0,
        disabled: colors.N0,
      },
      text: { default: colors.D1, disabled: 'black' },
      boxShadow: { default: boxShadow.shadow4.dark },
    },

    text: {
      bg: {
        default: colors.N0,
        hover: colors.N0,
        pressed: colors.N0,
        disabled: colors.N0,
      },
      text: { default: colors.D1, disabled: 'black' },
      boxShadow: { default: boxShadow.shadow4.dark },
    },

      default: {
        bg: {
          default: colors.TRANSPARENT,
          hover: colors.TRANSPARENT,
          pressed: colors.TRANSPARENT,
          disabled: colors.TRANSPARENT,
        },
        text: { default: colors.N140, disabled: colors.N140A40 },
        border: {
          default: colors.N160,
          hover: colors.N160,
          pressed: colors.N0,
          disabled: colors.N160A40,
        },
      },
      ghost: {
        bg: {
          default: colors.TRANSPARENT,
          hover: colors.N30,
          pressed: colors.N0,
          disabled: colors.N80A40,
          loading: colors.N90,
        },
        text: {
          default: colors.N140,
          hover: colors.N190,
          pressed: colors.N190,
          disabled: colors.N0A40,
          loading: colors.N120,
        },
      },
   
      delete: {
        bg: {
          default: colors.R20,
          hover: colors.R30,
          pressed: colors.R40,
          disabled: colors.R10,
        },
        text: { default: colors.N0, disabled: colors.N0 },
        boxShadow: { hover: boxShadow.shadow3.dark },
      },
    },
    checkbox: {
      border: {
        active: colors.Y10,
        inActive: colors.D40,
        disabled: colors.N190A05,
      },
      bg: { active: colors.D51, inActive: colors.D51, disabled: colors.N190A03 },
    },
    radio: {
      border: { active: colors.O10, noActive: colors.D40 },
      bg: { active: colors.O10 },
    },
    bg: { default: colors.N250, primary: colors.B20, multiSelect: colors.N60 },
    background: {
      primary: colors.D1,
      secondary: colors.D52,
      third: colors.D52,
      fourth: colors.G1,
      fifth: colors.G1,
      sixth: colors.D4,
    },

    boxShadow: {
      primary: boxShadow.shadow6.dark,
      secondary: boxShadow.shadow7.dark,
    },
    mode: 'dark' as ThemeModeType,
  };

  export const themes = {
    light: { ...colors, ...lightTheme } as const,
    dark: { ...colors, ...darkTheme } as const,
  };

  export type ThemeModeType = 'light' | 'dark';
  export type ThemeType = typeof themes.light;
  export type TextColor = keyof typeof lightTheme.text;
  export type ButtonType = keyof typeof themes.light.btn;
