import { createGlobalStyle } from '@styles';
import { fontFaces, fonts } from './fonts';

const GlobalStyles = createGlobalStyle`
  /* TODO: */
  ${fontFaces()}

  *,
  *::before,
  *::after {
    margin: 0px;
    padding: 0px;
    box-sizing: border-box;
    -webkit-tap-highlight-color: transparent;
  }
  html {
  }
  body {
    font-family: ${fonts.primary};
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    overflow-x: hidden;
    background-color: rgba(209, 211, 212, 0.7);;
    transition: background-color var(--theme-transition);
  }
  :root {
    --theme-transition: 300ms ease;
  }
  /* ::selection {
    opacity: 1;
    color: white;
    background-color: #da92fc;
  } */
  #__next { 
  }
  h1, h5, h6, h2, h3, h4, p, span  {
    margin: 0;
  }
  a {
    color: inherit;
    text-decoration: none;
  }
  li, ul {
    list-style: none;
  }
  picture {
    display: contents;
  }
  img {
    // width: 100%;
    // display: block;
    user-select: none;
    -webkit-user-drag: none;
  }
  button {
    font-family: inherit;
    white-space: nowrap;
    user-select: none;
    appearance: none;
    background: none;
    outline: none;
    border: none;
    cursor: pointer;
  }
  input[type='number']::-webkit-inner-spin-button,
  input[type='number']::-webkit-outer-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }
  .scroll-hidden {
    overflow: hidden;
    position: fixed;
    width: 100%;
    height: 100%;
  }
  .srOnly {
    border: 0 !important;
    clip: rect(1px, 1px, 1px, 1px) !important;
    clip-path: inset(50%) !important;
    height: 1px !important;
    margin: -1px !important;
    overflow: hidden !important;
    padding: 0 !important;
    position: absolute !important;
    width: 1px !important;
    white-space: nowrap !important;
  }
  .leaflet-container {
    cursor: crosshair;
  }
  .status {
    font-weight: 300;
    font-size: 16px;
    line-height: 20px;
    color: #00909E;
    padding-top: 3px;
    text-overflow: ellipsis;
      overflow: hidden;
      white-space: nowrap;
  }
  .status-disable{
    color:red
  }
  .price {
    color: #00C11F;
    font-size: 20px;
    line-height: 23px;
   
  }

  .sr-only {
    display: none
  }

  .ant-layout{
    background-color: #fff;
  }
  .ant-menu {
    background: unset;
  }
  .ant-menu-horizontal {
    border: unset
  }
  .ant-menu-item{
    padding: unset;
  }
  
    .ant-menu-item-selected a {
      color: #fff
    }
    .ant-menu-item-active{
      color: #fff

    }
    .ant-layout-sider{
      background: unset;
    } 

    .ant-menu:not(.ant-menu-horizontal) .ant-menu-item-selected {
    background-color: unset;
}
.ant-menu-item-selected a {
    color: unset ;
}
.ant-menu-vertical{
  border-right: unset;
}
.ant-collapse-ghost > .ant-collapse-item > .ant-collapse-content > .ant-collapse-content-box {
    padding-top: unset;
    padding-bottom: unset;
}

.ant-collapse-content > .ant-collapse-content-box {
    padding: unset;
}
a:hover {
  color:unset
}
.ant-dropdown-menu {
    position: relative;
    margin: 0;
    padding: 4px 0;
    text-align: left;
    list-style-type: none;
    background-color: #fff;
    background-clip: padding-box;
    border-radius: 2px;
    outline: none;
    box-shadow: 0 3px 6px -4px rgb(0 0 0 / 12%), 0 6px 16px 0 rgb(0 0 0 / 8%), 0 9px 28px 8px rgb(0 0 0 / 5%);
}

//move this styles from global styles

.categories-parent>.ant-collapse-item-active>{
        .ant-collapse-header{
          color: #00909E;
        }
      }
      .accordion-header-nested {
      

        .ant-collapse-content > .ant-collapse-content-box {
          padding-left: 15px;
          
        }
      
        .ant-collapse-header {
          padding: 10px 20px;
          }
        }

        .ant-collapse-item-active>
          .ant-collapse-header{

            font-weight: 600;
            

        }
        
      .categories-link{
        padding: 3px 0 3px 20px;
        display: list-item;
      }
      thead > tr > th {
         font-weight: bold !important;
      }
`;

export default GlobalStyles;
