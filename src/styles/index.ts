import * as styledComponents from 'styled-components';
import * as breakpoints from './breakpoints';
import { zIndex, StyleConstants } from './constants';
import { fonts, fontWeights } from './fonts';
import { themedColor, ThemeType } from './theme/themes';

// import ThemeProvider from './theme/ThemeProvider';

const {
  css,
  useTheme,
  keyframes,
  default: styled,
  createGlobalStyle,
} = styledComponents as unknown as 
styledComponents.ThemedStyledComponentsModule<ThemeType>;

export {
  css,
  fonts,
  zIndex,
  useTheme,
  keyframes,
  breakpoints,
  fontWeights,
  themedColor,
  // ThemeProvider,
  StyleConstants,
  createGlobalStyle,
};

export default styled;
