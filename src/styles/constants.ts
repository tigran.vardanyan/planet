export enum StyleConstants {
  CONTENT_HEIGHT= '100vh - 62px',
  NAV_BAR_HEIGHT = '4rem',
  GLOBAL_NAV_WIDTH = '80px',
  GLOBAL_NAV_HEIGHT = '126px',
  BORDER = '0px',
}

export const zIndex = {
  navigation: 900,
  serviceNav: 1200,
  logo: 1300,
  resize: 1100,
};

export const BRAND_CLASS_NAME = 'planet';
