function isEmpty(obj: Record<string, any>) {
  return Object.keys(obj).length === 0;
}

export default isEmpty;
