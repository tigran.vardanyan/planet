import { useState } from 'react';

export interface StackType<T> {
  top: () => T;
  pop: () => T;
  push: (item: T) => void;
  size: () => number;
  reset: () => void;
}

export const useStack = <T>(init: T[] = []) => {
  const [stack, setStack] = useState<T[]>(init);

  const pop = () => {
    const removedItem = stack[stack.length - 1];
    setStack(stack.slice(0, -1));
    return removedItem;
  };
  const push = (item: T) => setStack(stack => [...stack, item]);
  const top = () => stack[stack.length - 1];
  const size = () => stack.length;
  const reset = () => setStack(init);

  return {
    top,
    pop,
    push,
    size,
    reset,
  };
};
