import { useEffect, useMemo, useRef } from 'react';

import throttle from '@utils/throttle';

const useThrottle = <T extends Function>(func: T, ms: number = 200, dep: any[] = []) => {
  const cancelRef = useRef<Function>();

  const throttleFunc = useMemo(() => {
    const throttleFunc = throttle(func, 2000);
    cancelRef.current = throttleFunc.cancel;

    return throttleFunc;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, dep);

  useEffect(() => () => cancelRef.current?.(), []);

  return throttleFunc;
};

export default useThrottle;
