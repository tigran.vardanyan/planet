import { Modal, notification } from 'antd';
import { ServerError, UnauthorizedError, UnprocessableEntityError } from '../utils/errorHandler';

export const tryCatchProxy = (Class: any) => {
  const { prototype } = Class;

  if (Object.getOwnPropertyNames(prototype).length < 2) {
    return Class;
  }

  const handler = function handler(this: typeof Class, fn: (...args: any) => Promise<any>) {
    // eslint-disable-next-line func-names
    return async function (this: any, ...args: any) {
      try {
        return await fn.apply(this, args);
      } catch (info:any) {
        switch (true) {
          case info instanceof ServerError:
            Modal.error({
              title: 'Server error',
              content: 'Try again after few minutes.',
            });
            break;
          case info instanceof SyntaxError:
            Modal.error({
              title: 'Syntax error',
              content: info.message,
            });
            break;
          case info instanceof UnauthorizedError:
            notification.error({
              message: `Authorization error`,
              placement: 'bottomRight',
            });
            break;
          case info instanceof UnprocessableEntityError:
            notification.warning({
              message: `Validation error`,
              placement: 'bottomRight',
            });
            break;
          default:
            notification.info({
              message: `Some error`,
              placement: 'bottomRight',
            });
        }
        console.log('ERORRRR',info);
        

        throw info; // --- abort process's that waiting data from service
      }
    };
  };

  for (const property in Object.getOwnPropertyDescriptors(prototype)) {
    if (
      Object.prototype.hasOwnProperty.call(prototype, property) &&
      property !== 'constructor' &&
      typeof prototype[property] === 'function'
    ) {
      // eslint-disable-next-line no-param-reassign
      Class.prototype[property] = handler(Class.prototype[property]);
    }
  }

  return Class;
};
